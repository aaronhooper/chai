const chai = require('chai');
const assert = chai.assert;

suite('Unit Tests', function () {
    suite('Basic Assertions', function () {
        test('#isNull, #isNotNull', function () {
            assert.isNull(null);
            assert.isNotNull(1);
        });
    });
});
